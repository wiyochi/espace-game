# Espace Game
Projet de jeu de l'espace ! (Game contest Isima 2019)

Codé en C++ avec [SFML](https://www.sfml-dev.org/ "Simple and Fast Multimedia Library") et [rapidjson](http://rapidjson.org/ "JSON Parser")

---
Mathieu Arquillière - Jérémy Zangla - Août 2019